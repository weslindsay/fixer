import Foundation

struct ComparisonLayoutSection {

    // MARK: Internal properties

    let title: String
    let rows: [ComparisonTableViewCell.Data]

}
