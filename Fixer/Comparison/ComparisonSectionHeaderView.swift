import Foundation
import UIKit

final class ComparisonSectionHeaderView: UIView {

    // MARK: Private properties

    private let titleLabel = UILabel()

    // MARK: Lifecycle

    init(title: String) {
        super.init(frame: .zero)

        backgroundColor = UIColor.smokeWhite

        titleLabel.textColor = UIColor.arsenicBlack
        titleLabel.text = title
        titleLabel.textAlignment = .center
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
