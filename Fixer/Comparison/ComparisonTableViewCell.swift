import Foundation
import UIKit

final class ComparisonTableViewCell: UITableViewCell {

    // MARK: Internal types

    struct Data {
        let lhsCurrencyTitle: String
        let lhsCurrencyValue: String?
        let lhsFlagImage: UIImage?
        let rhsCurrencyTitle: String
        let rhsCurrencyValue: String?
        let rhsFlagImage: UIImage?
    }

    // MARK: Private properties

    private let lhsCurrencyValueLabel = UILabel()
    private let lhsCurrencyTitleLabel = UILabel()
    private let lhsFlagImageView = UIImageView()
    private let rhsCurrencyValueLabel = UILabel()
    private let rhsCurrencyTitleLabel = UILabel()
    private let rhsFlagImageView = UIImageView()
    private let separaterView = UIView()

    private struct Sizes {
        static let imageSize: CGFloat = 42
        static let verticalInset: CGFloat = 40
        static let horizontalInset: CGFloat = 20
        static let offset: CGFloat = 15
    }

    // MARK: Lifecycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        backgroundColor = UIColor.mistWhite

        selectionStyle = .none

        lhsFlagImageView.contentMode = .scaleAspectFit
        contentView.addSubview(lhsFlagImageView)
        lhsFlagImageView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(Sizes.verticalInset)
            make.leading.equalToSuperview().inset(Sizes.horizontalInset)
            make.height.width.equalTo(Sizes.imageSize)
        }

        lhsCurrencyTitleLabel.textColor = UIColor.graniteGrey
        lhsCurrencyTitleLabel.font = UIFont.systemFont(ofSize: 12, weight: .light)
        contentView.addSubview(lhsCurrencyTitleLabel)
        lhsCurrencyTitleLabel.snp.makeConstraints { make in
            make.leading.equalTo(lhsFlagImageView.snp.trailing).offset(Sizes.offset)
            make.centerY.equalTo(lhsFlagImageView)
        }

        lhsCurrencyValueLabel.textColor = UIColor.arsenicBlack
        lhsCurrencyValueLabel.font = UIFont.systemFont(ofSize: 24, weight: .light)
        contentView.addSubview(lhsCurrencyValueLabel)
        lhsCurrencyValueLabel.snp.makeConstraints { make in
            make.top.equalTo(lhsFlagImageView.snp.bottom).offset(Sizes.offset)
            make.leading.equalTo(lhsFlagImageView)
            make.bottom.equalToSuperview().inset(Sizes.verticalInset)
        }

        rhsFlagImageView.contentMode = .scaleAspectFit
        contentView.addSubview(rhsFlagImageView)
        rhsFlagImageView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(Sizes.verticalInset)
            make.trailing.equalToSuperview().inset(Sizes.horizontalInset)
            make.height.width.equalTo(Sizes.imageSize)
        }

        rhsCurrencyTitleLabel.textColor = UIColor.graniteGrey
        rhsCurrencyTitleLabel.font = UIFont.systemFont(ofSize: 12, weight: .light)
        contentView.addSubview(rhsCurrencyTitleLabel)
        rhsCurrencyTitleLabel.snp.makeConstraints { make in
            make.trailing.equalTo(rhsFlagImageView.snp.leading).offset(-Sizes.offset)
            make.centerY.equalTo(rhsFlagImageView)
        }

        rhsCurrencyValueLabel.textColor = UIColor.arsenicBlack
        rhsCurrencyValueLabel.font = UIFont.systemFont(ofSize: 24, weight: .light)
        contentView.addSubview(rhsCurrencyValueLabel)
        rhsCurrencyValueLabel.snp.makeConstraints { make in
            make.top.equalTo(rhsFlagImageView.snp.bottom).offset(Sizes.offset)
            make.trailing.equalTo(rhsFlagImageView)
            make.bottom.equalToSuperview().inset(Sizes.verticalInset)
        }
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: UITableViewCell overrides

    override func prepareForReuse() {
        super.prepareForReuse()

        separaterView.isHidden = false
    }

    // MARK: Internal methods

    func update(data: Data) {
        lhsFlagImageView.image = data.lhsFlagImage
        lhsCurrencyTitleLabel.text = data.lhsCurrencyTitle
        lhsCurrencyValueLabel.text = data.lhsCurrencyValue

        rhsFlagImageView.image = data.rhsFlagImage
        rhsCurrencyTitleLabel.text = data.rhsCurrencyTitle
        rhsCurrencyValueLabel.text = data.rhsCurrencyValue
    }
}
