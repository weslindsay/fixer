import Foundation
import RxCocoa
import RxSwift
import SnapKit
import UIKit

final class ComparisonViewController: UIViewController {

    // MARK: Internal types

    enum ViewMode {
        case loading
        case empty
        case comparison
    }

    // MARK: Private properties

    private let viewModel: ComparisonViewModel
    private let disposeBag = DisposeBag()
    private let currencyInputView = BaseCurrencyInputView()
    private let emptyView = CurrencyEmptyView()
    private let activityIndicatorView = UIActivityIndicatorView(style: .medium)
    private let refreshControl = UIRefreshControl()
    private let tableView = UITableView()

    private struct Sizes {
        static let sectionHeaderHeight: CGFloat = 36
        static let activityIndicatorSize: CGFloat = 36
        static let inset: CGFloat = 15
    }

    // MARK: Lifecycle

    init(viewModel: ComparisonViewModel) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: UIViewController overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Comparison"

        view.backgroundColor = UIColor.white

        activityIndicatorView.startAnimating()
        activityIndicatorView.tintColor = UIColor.smokeWhite
        view.addSubview(activityIndicatorView)
        activityIndicatorView.snp.makeConstraints { (make: ConstraintMaker) in
            make.center.equalToSuperview()
            make.height.width.equalTo(Sizes.activityIndicatorSize)
        }

        currencyInputView.isUserInteractionEnabled = false
        view.addSubview(currencyInputView)
        currencyInputView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.leading.trailing.equalToSuperview()
        }

        tableView.alpha = 0
        tableView.backgroundColor = UIColor.mistWhite
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(cellClass: ComparisonTableViewCell.self)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.top.equalTo(currencyInputView.snp.bottom)
            make.leading.bottom.trailing.equalToSuperview()
        }

        emptyView.alpha = 0
        view.addSubview(emptyView)
        emptyView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(Sizes.inset)
        }

        setupBindings()
    }

    // MARK: UIViewController overrides

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewModel.refreshRates()
    }

    // MARK: Private methods

    private func setupBindings() {
        viewModel
            .sections
            .subscribe(onNext: { [unowned self] _ in
                self.tableView.reloadData()
            })
            .disposed(by: disposeBag)

        viewModel
            .baseCurrencyData
            .subscribe(onNext: { [unowned self] optionalData in
                guard let data = optionalData else { return }

                self.currencyInputView.update(data: data)
            })
            .disposed(by: disposeBag)

        viewModel
            .viewMode
            .subscribe(onNext: { [unowned self] viewMode in
                self.updateView(for: viewMode)
            })
            .disposed(by: disposeBag)

        emptyView
            .retryTapped
            .subscribe(onNext: { [unowned self] _ in
                self.viewModel.refreshRates()
            })
            .disposed(by: disposeBag)
    }

    private func updateView(for mode: ViewMode) {
        var emptyViewAlpha: CGFloat = 0
        var tableViewAlpha: CGFloat = 0
        var activityIndicatorAlpha: CGFloat = 0

        switch mode {
        case .empty:
            emptyViewAlpha = 1
        case .loading:
            activityIndicatorAlpha = 1
        case .comparison:
            tableViewAlpha = 1
        }

        UIView.animate(withDuration: 0.25) {
            self.emptyView.alpha = emptyViewAlpha
            self.tableView.alpha = tableViewAlpha
            self.activityIndicatorView.alpha = activityIndicatorAlpha
        }
    }
}

extension ComparisonViewController: UITableViewDelegate {

    internal func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let section = viewModel.section(for: section) else { return nil }

        return ComparisonSectionHeaderView(title: section.title)
    }

    internal func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Sizes.sectionHeaderHeight
    }

    internal func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    internal func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
}

extension ComparisonViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sectionCount
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = viewModel.section(for: section) else { return 0 }

        return section.rows.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(cellClass: ComparisonTableViewCell.self, for: indexPath)

        guard let section = viewModel.section(for: indexPath.section),
            let rowData = section.rows[safe: indexPath.row] else {
                return cell
        }

        cell.update(data: rowData)

        return cell
    }
}
