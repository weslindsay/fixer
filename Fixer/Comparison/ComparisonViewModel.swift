import BrightFutures
import Foundation
import RxCocoa
import RxSwift

final class ComparisonViewModel {
    typealias Dependencies = HasExchangeRateClient

    // MARK: Internal types

    enum ComparisonError: Error {
        case baseRateUnavailable
        case serverError
    }

    // MARK: Internal properties

    var viewMode: Observable<ComparisonViewController.ViewMode> { return _viewMode.asObservable() }

    var sections: Observable<[ComparisonLayoutSection]> { return _sections.asObservable() }
    var sectionCount: Int { return _sections.value.count }

    var baseCurrencyData: Observable<BaseCurrencyInputView.Data?> { return _baseCurrencyData.asObservable() }

    // MARK: Private properties

    private let exchangeRateClient: ExchangeRateClientType
    private let comparisonOption: ComparisonOption
    private let startDate: Date
    private let responseDateFormatter: DateFormatter
    private let displayDateFormatter: DateFormatter

    private let disposeBag = DisposeBag()
    private let exchangeRateFormatter = NumberFormatter()

    private let _baseCurrency: BehaviorRelay<Currency>
    private let _baseCurrencyValue: BehaviorRelay<Decimal>
    private let _baseCurrencyData = BehaviorRelay<BaseCurrencyInputView.Data?>(value: nil)

    private let _viewMode = BehaviorRelay<ComparisonViewController.ViewMode>(value: .loading)

    private let _isFetching = BehaviorRelay<Bool>(value: true)
    private let _historicalRates = BehaviorRelay<HistoricalRates?>(value: nil)
    private let _sections = BehaviorRelay<[ComparisonLayoutSection]>(value: [])

    // MARK: Lifecycle

    init(dependencies: Dependencies, comparisonOption: ComparisonOption, startDate: Date = Date()) {
        self.exchangeRateClient = dependencies.exchangeRateClient
        self.comparisonOption = comparisonOption
        self.startDate = startDate

        self._baseCurrencyValue = BehaviorRelay<Decimal>(value: comparisonOption.baseAmount)
        self._baseCurrency = BehaviorRelay<Currency>(value: comparisonOption.baseCurrency)

        self.responseDateFormatter = DateFormatter()
        self.responseDateFormatter.dateFormat = "yyyy-MM-dd"

        self.displayDateFormatter = DateFormatter()
        self.displayDateFormatter.dateFormat = "MMM d, yyyy"

        self.exchangeRateFormatter.numberStyle = .decimal
        self.exchangeRateFormatter.maximumFractionDigits = 2

        setupBindings()
    }

    // MARK: Internal methods

    @discardableResult func refreshRates() -> Future<Void, ComparisonError> {
        _isFetching.accept(true)

        let requests: [Future<RatesResponse, ExchangeRateClientError>] = historicalDates()
            .map { date -> Future<RatesResponse, ExchangeRateClientError> in
                return self.exchangeRateClient.requestHistoricalRates(
                    on: date,
                    for: self.comparisonOption.lhsCurrency,
                    and: self.comparisonOption.rhsCurrency,
                    using: self.comparisonOption.baseCurrency
                ).flatMap { (ratesResponse: RatesResponse) -> Future<RatesResponse, ExchangeRateClientError> in
                    guard ratesResponse.success else { return Future(error: .serverError) }
                    return Future(value: ratesResponse)
                }
        }

        return requests
            .sequence()
            .mapError { serviceError -> ComparisonError in
                switch serviceError {
                case .deserialization:
                    return .baseRateUnavailable
                default:
                    return .serverError
                }
            }
            .flatMap { [weak self] responses -> Future<Void, ComparisonError> in
                guard let strongSelf = self else { return Future() }

                let updatedAt = strongSelf.startDate.timeIntervalSince1970
                let rates = strongSelf.map(responses: responses)

                let historicalRates = HistoricalRates(
                    updatedAt: updatedAt,
                    baseCurrency: strongSelf.comparisonOption.baseCurrency,
                    rates: rates
                )

                strongSelf._historicalRates.accept(historicalRates)

                return Future(value: ())
            }
            .onComplete { [weak self] _ in
                self?._isFetching.accept(false)
            }
    }

    func section(for sectionIndex: Int) -> ComparisonLayoutSection? {
        return _sections.value[safe: sectionIndex]
    }

    // MARK: Private methods

    private func setupBindings() {
        Observable.combineLatest(_historicalRates, _baseCurrencyValue)
            .map { [unowned self] (optionalRates, baseAmount) -> [ComparisonLayoutSection] in
                guard let historicalRates = optionalRates else { return [] }

                return historicalRates.rates.map { rate -> ComparisonLayoutSection in
                    return self.map(historicalRate: rate, with: baseAmount)
                }
            }
            .bind(to: _sections)
            .disposed(by: disposeBag)

        _baseCurrency
            .map { [unowned self] baseCurrency -> BaseCurrencyInputView.Data in
                let baseValue = NSDecimalNumber(decimal: self._baseCurrencyValue.value)

                return BaseCurrencyInputView.Data(
                    currencyTitle: baseCurrency.symbol,
                    currencyValue: self.exchangeRateFormatter.string(from: baseValue),
                    flagImage: UIImage(named: baseCurrency.symbol)
                )
            }
            .bind(to: _baseCurrencyData)
            .disposed(by: disposeBag)

        Observable.combineLatest(_isFetching, _historicalRates)
            .map { (isFetching, historicalRates) -> ComparisonViewController.ViewMode in
                switch (isFetching, historicalRates) {
                case (_, .some):
                    return .comparison
                case (false, .none):
                    return .empty
                case (true, .none):
                    return .loading
                }
            }
            .bind(to: _viewMode)
            .disposed(by: disposeBag)
    }

    private func map(responses: [RatesResponse]) -> [HistoricalRate] {
        let lhsCurrency = comparisonOption.lhsCurrency
        let rhsCurrency = comparisonOption.rhsCurrency

        return responses.compactMap { ratesResponse -> HistoricalRate? in
            guard let date = responseDateFormatter.date(from: ratesResponse.date),
                let lhsRateValue = ratesResponse.rates[lhsCurrency.symbol],
                let rhsRateValue = ratesResponse.rates[rhsCurrency.symbol] else {
                    return nil
            }

            let lhsRate = CurrencyRate(currency: lhsCurrency, value: lhsRateValue)
            let rhsRate = CurrencyRate(currency: rhsCurrency, value: rhsRateValue)

            return HistoricalRate(date: date, lhsRate: lhsRate, rhsRate: rhsRate)
        }
    }

    private func map(historicalRate: HistoricalRate, with baseAmount: Decimal) -> ComparisonLayoutSection {
        let sectionTitle = displayDateFormatter.string(from: historicalRate.date)
        let lhsRate = historicalRate.lhsRate
        let rhsRate = historicalRate.rhsRate

        let baseValue = NSDecimalNumber(decimal: baseAmount)
        let lhsValue = NSDecimalNumber(decimal: lhsRate.value)
        let lhsCurrencyValue: NSDecimalNumber = lhsValue.multiplying(by: baseValue)
        let rhsValue = NSDecimalNumber(decimal: rhsRate.value)
        let rhsCurrencyValue: NSDecimalNumber = rhsValue.multiplying(by: baseValue)

        let data = ComparisonTableViewCell.Data(
            lhsCurrencyTitle: lhsRate.currency.symbol,
            lhsCurrencyValue: exchangeRateFormatter.string(from: lhsCurrencyValue),
            lhsFlagImage: UIImage(named: lhsRate.currency.symbol),
            rhsCurrencyTitle: rhsRate.currency.symbol,
            rhsCurrencyValue: exchangeRateFormatter.string(from: rhsCurrencyValue),
            rhsFlagImage: UIImage(named: rhsRate.currency.symbol)
        )

        return ComparisonLayoutSection(title: sectionTitle, rows: [data])
    }

    private func historicalDates() -> [Date] {
        let calendar = Calendar.current
        let today = calendar.startOfDay(for: startDate)

        var historicalDates = [today]
        for day in 1...4 {
            if let date = Calendar.current.date(byAdding: .day, value: -day, to: today) {
                historicalDates.append(date)
            }
        }

        return historicalDates
    }
}
