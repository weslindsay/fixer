import Foundation
import RxCocoa
import RxSwift
import SnapKit
import UIKit

final class BaseCurrencyInputView: UIView {

    // MARK: Internal types

    struct Data {
        let currencyTitle: String
        let currencyValue: String?
        let flagImage: UIImage?
    }

    // MARK: Internal properties

    var value: Observable<String> { return currencyValueTextField.rx.text.map { $0 ?? "0" }.asObservable() }

    // MARK: Private properties

    private let currencyValueAccessoryView = InputAccessoryView()
    private let currencyValueTextField = UITextField()
    private let currencyTitleLabel = UILabel()
    private let flagImageView = UIImageView()
    private let disposeBag = DisposeBag()

    private let maximumInputLenght: Int = 10

    private struct Sizes {
        static let imageSize: CGFloat = 64
        static let verticalInset: CGFloat = 40
        static let horizontalInset: CGFloat = 20
        static let offset: CGFloat = 15
        static let separaterHeight: CGFloat = 0.5
    }

    // MARK: Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = UIColor.white

        flagImageView.contentMode = .scaleAspectFit
        addSubview(flagImageView)
        flagImageView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(Sizes.verticalInset)
            make.leading.equalToSuperview().inset(Sizes.horizontalInset)
            make.height.width.equalTo(Sizes.imageSize)
        }

        currencyTitleLabel.textColor = UIColor.graniteGrey
        currencyTitleLabel.font = UIFont.systemFont(ofSize: 20, weight: .light)
        currencyTitleLabel.textAlignment = .left
        addSubview(currencyTitleLabel)
        currencyTitleLabel.snp.makeConstraints { make in
            make.top.bottom.equalTo(flagImageView)
            make.leading.equalTo(flagImageView.snp.trailing).offset(Sizes.offset)
            make.width.lessThanOrEqualTo(flagImageView)
        }

        currencyValueTextField.keyboardType = .decimalPad
        currencyValueTextField.textColor = UIColor.jellyRed
        currencyValueTextField.font = UIFont.systemFont(ofSize: 28, weight: .light)
        currencyValueTextField.textAlignment = .right
        currencyValueTextField.delegate = self
        currencyValueTextField.inputAccessoryView = currencyValueAccessoryView
        addSubview(currencyValueTextField)
        currencyValueTextField.snp.makeConstraints { make in
            make.leading.equalTo(currencyTitleLabel.snp.trailing).offset(Sizes.offset)
            make.trailing.equalToSuperview().inset(Sizes.horizontalInset)
            make.top.bottom.equalTo(flagImageView)
        }

        let separaterView = UIView()
        separaterView.backgroundColor = UIColor.smokeWhite
        addSubview(separaterView)
        separaterView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.height.equalTo(Sizes.separaterHeight)
        }

        setupBindings()
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: Internal methods

    func update(data: Data) {
        currencyTitleLabel.text = data.currencyTitle
        currencyValueTextField.text = data.currencyValue
        flagImageView.image = data.flagImage
    }

    // MARK: Private methods

    private func setupBindings() {
        currencyValueAccessoryView
            .doneTapped
            .subscribe(onNext: { [unowned self] in
                self.currencyValueTextField.resignFirstResponder()
            })
            .disposed(by: disposeBag)
    }
}

extension BaseCurrencyInputView: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = textField.text, let textRange = Range(range, in: currentText) else { return true }

        let updatedText = currentText.replacingCharacters(in: textRange, with: string)

        return updatedText.count <= maximumInputLenght
    }
}
