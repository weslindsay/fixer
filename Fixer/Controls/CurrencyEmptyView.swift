import Foundation
import RxCocoa
import RxSwift
import SnapKit
import UIKit

final class CurrencyEmptyView: UIView {

    // MARK: Internal methods

    var retryTapped: Observable<Void> { return retryButton.rx.tap.asObservable() }

    // MARK: Private properties

    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()
    private let retryButton = UIButton()

    private struct Sizes {
        static let inset: CGFloat = 15
        static let buttonHeight: CGFloat = 44
    }

    // MARK: Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = UIColor.white

        titleLabel.textColor = UIColor.arsenicBlack
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .light)
        titleLabel.text = "Oh no!"
        titleLabel.textAlignment = .center
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview().inset(Sizes.inset)
        }

        subtitleLabel.textColor = UIColor.arsenicBlack
        subtitleLabel.font = UIFont.systemFont(ofSize: 14, weight: .light)
        subtitleLabel.text = "Something went wrong and we were unable to find the currencies at this moment."
        subtitleLabel.textAlignment = .center
        subtitleLabel.numberOfLines = 0
        addSubview(subtitleLabel)
        subtitleLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(Sizes.inset / 2)
            make.leading.trailing.equalToSuperview().inset(Sizes.inset)
        }

        retryButton.setTitleColor(UIColor.white, for: .normal)
        retryButton.setTitle("TRY AGAIN", for: .normal)
        retryButton.backgroundColor = UIColor.jellyRed
        addSubview(retryButton)
        retryButton.snp.makeConstraints { make in
            make.top.equalTo(subtitleLabel.snp.bottom).offset(Sizes.inset)
            make.leading.bottom.trailing.equalToSuperview().inset(Sizes.inset)
            make.height.equalTo(Sizes.buttonHeight)
        }
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
