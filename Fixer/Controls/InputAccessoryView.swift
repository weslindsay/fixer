import RxSwift
import SnapKit
import UIKit

final class InputAccessoryView: UIView {

    // MARK: Internal methods

    var doneTapped: Observable<Void> { return doneButton.rx.tap.asObservable() }

    // MARK: Private properties

    private let doneButton = UIButton()

    private struct Sizes {
        static let intrinsicHeight: CGFloat = 50
    }

    // MARK: Lifecycle

    override init(frame: CGRect) {
        super.init(frame: CGRect())

        translatesAutoresizingMaskIntoConstraints = false

        doneButton.setTitleColor(UIColor.white, for: .normal)
        doneButton.setTitle("DONE", for: .normal)
        doneButton.backgroundColor = UIColor.jellyRed
        addSubview(doneButton)
        doneButton.snp.makeConstraints { (make: ConstraintMaker) in
            make.edges.equalToSuperview()
        }
    }

    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: UIView overrides

    override var intrinsicContentSize: CGSize {
        return CGSize(width: bounds.width, height: Sizes.intrinsicHeight)
    }
}
