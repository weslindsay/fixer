import Foundation
import RxSwift
import RxCocoa
import UIKit

final class AppFlowCoordinator: FlowCoordinatorType {
    typealias Dependencies = HasExchangeRateClient

    // MARK: Private properties

    private let dependencies: Dependencies
    private let navigationController = UINavigationController()
    private let disposeBag = DisposeBag()

    // MARK: Lifecycle

    init(dependencies: Dependencies) {
        self.dependencies = dependencies

        self.navigationController.navigationBar.shadowImage = UIImage()
        self.navigationController.navigationBar.isTranslucent = false
        self.navigationController.navigationBar.backgroundColor = UIColor.white
        self.navigationController.navigationBar.tintColor = UIColor.jellyRed
        self.navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.arsenicBlack]
    }

    // MARK: FlowCoordinatorType methods

    func start() -> UIViewController {
        showLatestRatesViewController()

        return navigationController
    }

    // MARK: Private methods

    private func showLatestRatesViewController() {
        let viewModel = LatestRatesViewModel(dependencies: dependencies)
        let viewController = LatestRatesViewController(viewModel: viewModel)

        viewController
            .actions
            .subscribe(onNext: { [unowned self] action in
                switch action {
                case let .compareSelected(option):
                    self.showComparisonViewController(with: option)
                }
            })
            .disposed(by: disposeBag)

        navigationController.setViewControllers([viewController], animated: false)
    }

    private func showComparisonViewController(with comparisonOption: ComparisonOption) {
        let viewModel = ComparisonViewModel(dependencies: dependencies, comparisonOption: comparisonOption)
        let viewController = ComparisonViewController(viewModel: viewModel)

        navigationController.pushViewController(viewController, animated: true)
    }
}
