import Foundation
import UIKit

protocol FlowCoordinatorType {
    func start() -> UIViewController
}
