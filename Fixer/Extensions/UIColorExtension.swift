import Foundation
import UIKit

extension UIColor {

    open class var mistWhite: UIColor {
        return UIColor(red: 253.0 / 255.0, green: 253.0 / 255.0, blue: 253.0 / 255.0, alpha: 1.0)
    }

    open class var smokeWhite: UIColor {
        return UIColor(red: 220.0 / 255.0, green: 220.0 / 255.0, blue: 220.0 / 255.0, alpha: 1.0)
    }

    open class var arsenicBlack: UIColor {
        return UIColor(red: 73.0 / 255.0, green: 73.0 / 255.0, blue: 73.0 / 255.0, alpha: 1.0)
    }

    open class var graniteGrey: UIColor {
        return UIColor(red: 170.0 / 255.0, green: 170.0 / 255.0, blue: 170.0 / 255.0, alpha: 1.0)
    }

    open class var jellyRed: UIColor {
        return UIColor(red: 231.0 / 255.0, green: 91.0 / 255.0, blue: 86.0 / 255.0, alpha: 1.0)
    }
}
