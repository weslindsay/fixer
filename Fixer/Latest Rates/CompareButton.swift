import Foundation
import SnapKit
import UIKit

final class CompareButton: UIButton {

    // MARK: Internal types

    enum ButtonState: Equatable {
        case enabled
        case disabled
    }

    // MARK: Private Types

    private struct Opacity {
        static let enabled: CGFloat = 1.00
        static let highlighted: CGFloat = 0.75
        static let disabled: CGFloat = 0.35
    }

    private struct Sizes {
        static let intrinsicHeight: CGFloat = 50
    }

    // MARK: Lifecycle

    override init(frame: CGRect) {
        super.init(frame: .zero)

        setTitle("COMPARE", for: .normal)

        updateColors()
    }

    required public init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: UIButton overrides

    override public var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: Sizes.intrinsicHeight)
    }

    override public var isSelected: Bool { didSet { updateColors() } }
    override public var isHighlighted: Bool { didSet { updateColors() } }
    override public var isEnabled: Bool { didSet { updateColors() } }

    // MARK: Public functions

    public func update(state: ButtonState) {
        isEnabled = state == .enabled

        updateColors()
    }

    // MARK: Private methods

    private func updateColors() {
        switch (isEnabled, isSelected, isHighlighted) {
        case (false, _, _):
            tintColor = UIColor.jellyRed.withAlphaComponent(Opacity.disabled)
            setTitleColor(UIColor.white.withAlphaComponent(Opacity.disabled), for: .normal)
        case (true, true, _), (true, _, true):
            tintColor = UIColor.jellyRed.withAlphaComponent(Opacity.highlighted)
            setTitleColor(UIColor.white.withAlphaComponent(Opacity.highlighted), for: .normal)
        default:
            tintColor = UIColor.jellyRed
            setTitleColor(UIColor.white, for: .normal)
        }

        let backgroundColorFade = CABasicAnimation(keyPath: "backgroundColor")
        backgroundColorFade.fromValue = layer.backgroundColor
        backgroundColorFade.toValue = tintColor.cgColor
        backgroundColorFade.duration = 0.15

        layer.add(backgroundColorFade, forKey: "backgroundColorFade")
        backgroundColor = tintColor
    }
}
