import Foundation
import RxCocoa
import RxSwift
import SnapKit
import UIKit

final class CompareButtonView: UIView {

    // MARK: Internal properties

    var compareTapped: Observable<Void> { return compareButton.rx.tap.asObservable() }

    // MARK: Private properties

    private let compareButton = CompareButton()

    // MARK: Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(compareButton)
        compareButton.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: Internal methods

    func update(buttonState: CompareButton.ButtonState) {
        compareButton.update(state: buttonState)
    }
}
