import Foundation
import UIKit

final class LatestRateTableViewCell: UITableViewCell {

    // MARK: Internal types

    struct Data {
        let currencyTitle: String
        let currencyValue: String?
        let flagImage: UIImage?
        let hidesSeparater: Bool
    }

    // MARK: Private properties

    private let currencyValueLabel = UILabel()
    private let currencyTitleLabel = UILabel()
    private let flagImageView = UIImageView()
    private let separaterView = UIView()

    private struct Sizes {
        static let imageSize: CGFloat = 64
        static let verticalInset: CGFloat = 40
        static let horizontalInset: CGFloat = 20
        static let offset: CGFloat = 15
        static let separaterHeight: CGFloat = 0.5
    }

    // MARK: Lifecycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.jellyRed.withAlphaComponent(0.1)
        selectedBackgroundView = backgroundView

        backgroundColor = UIColor.mistWhite

        flagImageView.contentMode = .scaleAspectFit
        contentView.addSubview(flagImageView)
        flagImageView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(Sizes.verticalInset)
            make.leading.equalToSuperview().inset(Sizes.horizontalInset)
            make.height.width.equalTo(Sizes.imageSize)
        }

        currencyTitleLabel.textColor = UIColor.graniteGrey
        currencyTitleLabel.font = UIFont.systemFont(ofSize: 20, weight: .light)
        currencyTitleLabel.textAlignment = .left
        contentView.addSubview(currencyTitleLabel)
        currencyTitleLabel.snp.makeConstraints { make in
            make.top.bottom.equalTo(flagImageView)
            make.leading.equalTo(flagImageView.snp.trailing).offset(Sizes.offset)
        }

        currencyValueLabel.textColor = UIColor.arsenicBlack
        currencyValueLabel.font = UIFont.systemFont(ofSize: 28, weight: .light)
        currencyValueLabel.textAlignment = .right
        currencyValueLabel.adjustsFontSizeToFitWidth = true
        contentView.addSubview(currencyValueLabel)
        currencyValueLabel.snp.makeConstraints { make in
            make.leading.equalTo(currencyTitleLabel.snp.trailing).offset(Sizes.offset)
            make.trailing.equalToSuperview().inset(Sizes.horizontalInset)
            make.top.bottom.equalTo(flagImageView)
        }

        separaterView.backgroundColor = UIColor.smokeWhite
        addSubview(separaterView)
        separaterView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.height.equalTo(Sizes.separaterHeight)
        }
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: UITableViewCell overrides

    override func prepareForReuse() {
        super.prepareForReuse()

        currencyTitleLabel.text = nil
        currencyValueLabel.text = nil
        flagImageView.image = nil
        separaterView.isHidden = false
    }

    // MARK: Internal methods

    func update(data: Data) {
        currencyTitleLabel.text = data.currencyTitle
        currencyValueLabel.text = data.currencyValue
        flagImageView.image = data.flagImage
        separaterView.isHidden = data.hidesSeparater
    }
}
