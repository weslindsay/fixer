import Foundation
import RxCocoa
import RxSwift
import SnapKit
import UIKit

final class LatestRatesViewController: UIViewController {

    // MARK: Internal types

    enum Actions {
        case compareSelected(ComparisonOption)
    }

    enum SelectionMode {
        case normal
        case selection
    }

    enum ViewMode {
        case loading
        case empty
        case latestRates
    }

    // MARK: Internal properites

    var actions: Observable<Actions> {
        return _actions.asObservable()
    }

    // MARK: Private properties

    private let viewModel: LatestRatesViewModel
    private let currencyInputView = BaseCurrencyInputView()
    private let refreshControl = UIRefreshControl()
    private let tableView = UITableView()
    private let emptyView = CurrencyEmptyView()
    private let activityIndicatorView = UIActivityIndicatorView(style: .medium)
    private let compareButtonView = CompareButtonView()
    private let selectButton = UIBarButtonItem()
    private let cancelButton = UIBarButtonItem()
    private let disposeBag = DisposeBag()

    private let _actions = PublishSubject<Actions>()

    private struct Sizes {
        static let activityIndicatorSize: CGFloat = 36
        static let inset: CGFloat = 15
    }

    // MARK: Lifecycle

    init(viewModel: LatestRatesViewModel) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: UIViewController overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Latest Rates"

        view.backgroundColor = UIColor.white

        selectButton.title = "Select"
        selectButton.tintColor = UIColor.jellyRed

        cancelButton.title = "Cancel"
        cancelButton.tintColor = UIColor.jellyRed

        navigationItem.backBarButtonItem = UIBarButtonItem()
        navigationItem.rightBarButtonItem = selectButton

        view.addSubview(currencyInputView)
        currencyInputView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.leading.trailing.equalToSuperview()
        }

        view.addSubview(compareButtonView)
        compareButtonView.snp.makeConstraints { make in
            make.top.equalTo(view.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }

        activityIndicatorView.startAnimating()
        activityIndicatorView.tintColor = UIColor.smokeWhite
        view.addSubview(activityIndicatorView)
        activityIndicatorView.snp.makeConstraints { (make: ConstraintMaker) in
            make.center.equalToSuperview()
            make.height.width.equalTo(Sizes.activityIndicatorSize)
        }

        emptyView.alpha = 0
        view.addSubview(emptyView)
        emptyView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(Sizes.inset)
        }

        tableView.refreshControl = refreshControl
        refreshControl.tintColor = UIColor.smokeWhite
        tableView.alpha = 0
        tableView.backgroundColor = UIColor.mistWhite
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(cellClass: LatestRateTableViewCell.self)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.top.equalTo(currencyInputView.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(compareButtonView.snp.top)
        }

        DispatchQueue.main.async {
            self.setupBindings()
        }
    }

    // MARK: UIViewController overrides

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewModel.resetSelectionMode()

        refreshRates()
    }

    // MARK: Private methods

    private func setupBindings() {
        refreshControl.addTarget(self, action: #selector(refreshRates), for: .valueChanged)

        viewModel
            .rows
            .subscribe(onNext: { [unowned self] _ in
                self.tableView.reloadData()
            })
            .disposed(by: disposeBag)

        viewModel
            .baseCurrencyData
            .subscribe(onNext: { [unowned self] optionalData in
                guard let data = optionalData else { return }

                self.currencyInputView.update(data: data)
            })
            .disposed(by: disposeBag)

        viewModel
            .selectionMode
            .distinctUntilChanged()
            .subscribe(onNext: { [unowned self] selectionMode in
                self.updateView(for: selectionMode)
            })
            .disposed(by: disposeBag)

        viewModel
            .compareButtonState
            .subscribe(onNext: { [unowned self] state in
                self.compareButtonView.update(buttonState: state)
            })
            .disposed(by: disposeBag)

        viewModel
            .viewMode
            .subscribe(onNext: { [unowned self] viewMode in
                self.updateView(for: viewMode)
            })
            .disposed(by: disposeBag)

        selectButton
            .rx
            .tap.subscribe(onNext: { [unowned self] _ in
                self.view.endEditing(true)
                self.viewModel.toggleSelection()
            })
            .disposed(by: disposeBag)

        cancelButton
            .rx
            .tap.subscribe(onNext: { [unowned self] _ in
                self.viewModel.toggleSelection()
            })
            .disposed(by: disposeBag)

        currencyInputView
            .value
            .bind(to: viewModel.baseCurrencyInput)
            .disposed(by: disposeBag)

        compareButtonView
            .compareTapped
            .subscribe(onNext: { [unowned self] _ in
                guard let comparisonOption = self.viewModel.comparisonOption() else {
                    return
                }

                self._actions.onNext(.compareSelected(comparisonOption))
            })
            .disposed(by: disposeBag)

        emptyView
            .retryTapped
            .subscribe(onNext: { [unowned self] _ in
                self.viewModel.refreshRates()
            })
            .disposed(by: disposeBag)
    }

    private func updateView(for mode: ViewMode) {
        var emptyViewAlpha: CGFloat = 0
        var tableViewAlpha: CGFloat = 0
        var activityIndicatorAlpha: CGFloat = 0

        switch mode {
        case .empty:
            emptyViewAlpha = 1
        case .loading:
            activityIndicatorAlpha = 1
        case .latestRates:
            tableViewAlpha = 1
        }

        UIView.animate(withDuration: 0.25) {
            self.emptyView.alpha = emptyViewAlpha
            self.tableView.alpha = tableViewAlpha
            self.activityIndicatorView.alpha = activityIndicatorAlpha
        }
    }

    private func updateView(for selectionMode: SelectionMode) {
        tableView.allowsMultipleSelection = selectionMode == .selection
        tableView.clearSelections(animated: true)

        switch selectionMode {
        case .normal:
            navigationItem.setRightBarButton(selectButton, animated: true)
            hideCompareView()
        case .selection:
            navigationItem.setRightBarButton(cancelButton, animated: true)
            showCompareView()
        }

        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }

    private func hideCompareView() {
        compareButtonView.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(view.snp.bottom)
        }
    }

    private func showCompareView() {
        compareButtonView.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        }
    }

    @objc private func refreshRates() {
        viewModel
            .refreshRates()
            .onComplete { [weak self] _ in
                self?.refreshControl.endRefreshing()
            }
    }
}

extension LatestRatesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        guard viewModel.currentSelectionMode == .selection else { return nil }

        guard let selectedIndexes = tableView.indexPathsForSelectedRows else { return indexPath }

        return selectedIndexes.count < 2 ? indexPath : nil
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectCurrency(at: indexPath)
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        viewModel.deselectCurrency(at: indexPath)
    }
}

extension LatestRatesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rowCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let rowData = viewModel.rowData(for: indexPath) else { return UITableViewCell() }

        let cell = tableView.dequeueReusableCell(cellClass: LatestRateTableViewCell.self, for: indexPath)
        cell.update(data: rowData)

        return cell
    }
}
