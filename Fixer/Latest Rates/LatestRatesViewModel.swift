import BrightFutures
import Foundation
import RxCocoa
import RxSwift

final class LatestRatesViewModel {
    typealias Dependencies = HasExchangeRateClient

    // MARK: Internal types

    enum LatestRatesError: Error {
        case baseRateUnavailable
        case serverError
    }

     // MARK: Internal properties

    let baseCurrencyInput = BehaviorRelay<String>(value: "1")
    var baseCurrencyData: Observable<BaseCurrencyInputView.Data?> { return _baseCurrencyData.asObservable() }

    var rows: Observable<[LatestRateTableViewCell.Data]> { return _rows.asObservable() }
    var rowCount: Int { return _rows.value.count }

    var viewMode: Observable<LatestRatesViewController.ViewMode> { return _viewMode.asObservable() }

    var currentSelectionMode: LatestRatesViewController.SelectionMode { return _selectionMode.value }
    var selectionMode: Observable<LatestRatesViewController.SelectionMode> { return _selectionMode.asObservable() }

    var compareButtonState: Observable<CompareButton.ButtonState> { return _compareButtonState.asObservable() }

    // MARK: Private properties

    private let exchangeRateClient: ExchangeRateClientType
    private let disposeBag = DisposeBag()
    private let exchangeRateFormatter = NumberFormatter()

    private let currencies: [Currency] = [
        .USD, .JPY, .GBP, .AUD, .CAD, .CHF, .CNY, .SEK, .NZD
    ]

    private let _baseCurrency = BehaviorRelay<Currency>(value: Currency.EUR)
    private let _baseCurrencyValue = BehaviorRelay<Decimal>(value: 1)
    private let _baseCurrencyData = BehaviorRelay<BaseCurrencyInputView.Data?>(value: nil)

    private let _latestRates = BehaviorRelay<LatestRates?>(value: nil)
    private let _rows = BehaviorRelay<[LatestRateTableViewCell.Data]>(value: [])

    private let _viewMode = BehaviorRelay<LatestRatesViewController.ViewMode>(value: .loading)
    private let _selectionMode = BehaviorRelay<LatestRatesViewController.SelectionMode>(value: .normal)

    private let _isFetching = BehaviorRelay<Bool>(value: true)
    private let _selectedCurrencies = BehaviorRelay<[Currency]>(value: [])
    private let _compareButtonState = BehaviorRelay<CompareButton.ButtonState>(value: .disabled)

    // MARK: Lifecycle

    init(dependencies: Dependencies) {
        self.exchangeRateClient = dependencies.exchangeRateClient
        self.exchangeRateFormatter.numberStyle = .decimal
        self.exchangeRateFormatter.maximumFractionDigits = 2

        setupBindings()
    }

    // MARK: Internal methods

    @discardableResult func refreshRates() -> Future<Void, LatestRatesError> {
        _isFetching.accept(true)

        return exchangeRateClient
            .requestLatestRates(for: currencies, using: _baseCurrency.value)
            .mapError { serviceError -> LatestRatesError in
                switch serviceError {
                case .deserialization:
                    return .baseRateUnavailable
                default:
                    return .serverError
                }
            }
            .flatMap { [weak self] (response: RatesResponse) -> Future<Void, LatestRatesError> in
                guard response.success else { return Future(error: .serverError) }

                guard let latestRates = self?.map(response: response) else {
                    return Future(error: .baseRateUnavailable)
                }

                self?._latestRates.accept(latestRates)

                return Future(value: ())
            }
            .onComplete { [weak self] _ in
                self?._isFetching.accept(false)
            }
    }

    func rowData(for indexPath: IndexPath) -> LatestRateTableViewCell.Data? {
        return _rows.value[safe: indexPath.row]
    }

    func selectCurrency(at indexPath: IndexPath) {
        guard let currencyRate = _latestRates.value?.rates[safe: indexPath.row] else {
            return
        }

        var currencies = _selectedCurrencies.value
        currencies.append(currencyRate.currency)
        _selectedCurrencies.accept(currencies)
    }

    func deselectCurrency(at indexPath: IndexPath) {
        guard let currencyRate = _latestRates.value?.rates[safe: indexPath.row] else {
            return
        }

        var currencies = _selectedCurrencies.value
        currencies.removeAll { currency -> Bool in
            return currency == currencyRate.currency
        }

        _selectedCurrencies.accept(currencies)
    }

    func comparisonOption() -> ComparisonOption? {
        guard _selectedCurrencies.value.count == 2 else { return nil }

        guard let lhsCurrency = _selectedCurrencies.value.first,
            let rhsCurrency = _selectedCurrencies.value.last else {
            return nil
        }

        return ComparisonOption(
            baseCurrency: _baseCurrency.value,
            baseAmount: _baseCurrencyValue.value,
            lhsCurrency: lhsCurrency,
            rhsCurrency: rhsCurrency
        )
    }

    func toggleSelection() {
        switch _selectionMode.value {
        case .normal:
            _selectionMode.accept(.selection)
        case .selection:
            _selectionMode.accept(.normal)
        }
    }

    func resetSelectionMode() {
        _selectionMode.accept(.normal)
    }

    // MARK: Private methods

    private func setupBindings() {
        Observable.combineLatest(_latestRates, _baseCurrencyValue)
            .map { [unowned self] optionalLatestRates, baseAmount -> [LatestRateTableViewCell.Data] in
                guard let latestRates = optionalLatestRates else { return [] }

                return latestRates.rates.enumerated().map { (index, currencyRate) -> LatestRateTableViewCell.Data in
                    let isLastRow = index == latestRates.rates.count - 1
                    let rate = NSDecimalNumber(decimal: currencyRate.value)
                    let baseValue = NSDecimalNumber(decimal: baseAmount)
                    let currencyValue: NSDecimalNumber = rate.multiplying(by: baseValue)
                    let value = self.exchangeRateFormatter.string(from: currencyValue)

                    return LatestRateTableViewCell.Data(
                        currencyTitle: currencyRate.currency.symbol,
                        currencyValue: value,
                        flagImage: UIImage(named: currencyRate.currency.symbol),
                        hidesSeparater: isLastRow
                    )
                }
            }
            .bind(to: _rows)
            .disposed(by: disposeBag)

        _baseCurrency
            .map { [unowned self] baseCurrency -> BaseCurrencyInputView.Data in
                return BaseCurrencyInputView.Data(
                    currencyTitle: baseCurrency.symbol,
                    currencyValue: self.baseCurrencyInput.value,
                    flagImage: UIImage(named: baseCurrency.symbol)
                )
            }
            .bind(to: _baseCurrencyData)
            .disposed(by: disposeBag)

        baseCurrencyInput
            .map { input -> Decimal in
                guard let decimalValue = Decimal(string: input) else {
                    return 0
                }

                return decimalValue
            }
            .bind(to: _baseCurrencyValue)
            .disposed(by: disposeBag)

        _selectionMode
            .distinctUntilChanged()
            .subscribe(onNext: { [unowned self] _ in
                self._selectedCurrencies.accept([])
            })
            .disposed(by: disposeBag)

        _selectedCurrencies
            .map { selectedCurrencies -> CompareButton.ButtonState in
                return selectedCurrencies.count == 2 ? .enabled : .disabled
            }
            .bind(to: _compareButtonState)
            .disposed(by: disposeBag)

        Observable.combineLatest(_isFetching, _latestRates)
            .map { (isFetching, latestRates) -> LatestRatesViewController.ViewMode in
                switch (isFetching, latestRates) {
                case (_, .some):
                    return .latestRates
                case (false, .none):
                    return .empty
                case (true, .none):
                    return .loading
                }
            }
            .bind(to: _viewMode)
            .disposed(by: disposeBag)
    }

    private func map(response: RatesResponse) -> LatestRates? {
        guard let baseCurrency = Currency(rawValue: response.base) else { return nil }

        let rates = response.rates.compactMap { (symbol, value) -> CurrencyRate? in
            guard let currency = Currency(rawValue: symbol) else { return nil}

            return CurrencyRate(currency: currency, value: value)
        }

        let sortedRates = rates.sorted { return $0.currency.symbol < $1.currency.symbol }

        return LatestRates(updatedAt: response.timestamp, baseCurrency: baseCurrency, rates: sortedRates)
    }
}
