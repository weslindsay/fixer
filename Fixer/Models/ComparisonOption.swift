import Foundation

struct ComparisonOption {
    let baseCurrency: Currency
    let baseAmount: Decimal
    let lhsCurrency: Currency
    let rhsCurrency: Currency
}
