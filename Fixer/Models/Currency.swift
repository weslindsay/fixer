import Foundation

enum Currency: String, Decodable {
    case EUR
    case USD
    case JPY
    case GBP
    case AUD
    case CAD
    case CHF
    case CNY
    case SEK
    case NZD

    var symbol: String {
        return self.rawValue
    }
}
