import Foundation

struct CurrencyRate {
    let currency: Currency
    let value: Decimal
}
