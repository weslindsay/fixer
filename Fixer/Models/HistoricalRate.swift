import Foundation

struct HistoricalRate {
    let date: Date
    let lhsRate: CurrencyRate
    let rhsRate: CurrencyRate
}
