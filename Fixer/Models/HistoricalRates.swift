import Foundation

struct HistoricalRates {
    let updatedAt: TimeInterval
    let baseCurrency: Currency
    let rates: [HistoricalRate]
}
