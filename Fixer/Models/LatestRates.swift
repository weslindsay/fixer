import Foundation

struct LatestRates {
    let updatedAt: TimeInterval
    let baseCurrency: Currency
    let rates: [CurrencyRate]
}
