import Foundation

struct RatesResponse: Decodable {
    let success: Bool
    let timestamp: TimeInterval
    let date: String
    let base: String
    let rates: [String: Decimal]
}
