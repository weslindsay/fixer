import UIKit

final class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    // MARK: Internal properties

    var window: UIWindow?

    // MARK: Private properties

    private let appConstants = AppConstants()

    private var appDependancies: AppDependencies!
    private var appFlowCoordinator: AppFlowCoordinator!

    // MARK: Lifecyle

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }

        appDependancies = AppDependencies(appConstants: appConstants)
        appFlowCoordinator = AppFlowCoordinator(dependencies: appDependancies)

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = appFlowCoordinator.start()
        window?.makeKeyAndVisible()
        window?.windowScene = windowScene
    }
}

