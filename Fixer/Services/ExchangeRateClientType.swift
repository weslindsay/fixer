import BrightFutures
import Foundation

protocol ExchangeRateClientType {
    func requestLatestRates<Response: Decodable>(
        for currencies: [Currency],
        using baseCurrency: Currency) -> Future<Response, ExchangeRateClientError>

    func requestHistoricalRates<Response: Decodable>(
        on date: Date,
        for firstCurrency: Currency,
        and secondCurrency: Currency,
        using baseCurrency: Currency) -> Future<Response, ExchangeRateClientError>
}

enum ExchangeRateClientError: Error {
    case deserialization
    case serverError
    case other(Error)
}
