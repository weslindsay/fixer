import BrightFutures
import Foundation
import Networking

final class FixerClient: ExchangeRateClientType {

    // MARK: Private properties

    private let appConstants: AppConstants
    private let networkClient: Networking
    private let dateFormatter: DateFormatter

    // MARK: Lifecycle

    init(appConstants: AppConstants) {
        self.appConstants = appConstants
        self.networkClient = Networking(baseURL: appConstants.fixerApiBaseURL)
        self.dateFormatter = DateFormatter()
        self.dateFormatter.dateFormat = "yyyy-MM-dd"
    }

    // MARK: ExchangeRateClientType methods

    func requestLatestRates<Response: Decodable>(
        for currencies: [Currency],
        using baseCurrency: Currency) -> Future<Response, ExchangeRateClientError> {

        let parameters = [
            "access_key": appConstants.fixerAceessKey,
            "base": baseCurrency.symbol,
            "symbols": currencies.map { $0.symbol }.joined(separator: ",")
        ]

        return get(path: "latest", with: parameters)
            .flatMap { (jsonResponse) -> Future<Response, ExchangeRateClientError> in
                do {
                    let response = try JSONDecoder().decode(Response.self, from: jsonResponse.data)
                    return Future(value: response)
                } catch {
                    return Future(error: .deserialization)
                }
            }
    }

    func requestHistoricalRates<Response>(
        on date: Date,
        for firstCurrency: Currency,
        and secondCurrency: Currency,
        using baseCurrency: Currency) -> Future<Response, ExchangeRateClientError> where Response : Decodable {

        let parameters = [
            "access_key": appConstants.fixerAceessKey,
            "base": baseCurrency.symbol,
            "symbols": "\(firstCurrency.symbol), \(secondCurrency.symbol)"
        ]

        return get(path: dateFormatter.string(from: date), with: parameters)
            .flatMap { (jsonResponse) -> Future<Response, ExchangeRateClientError> in
                do {
                    let response = try JSONDecoder().decode(Response.self, from: jsonResponse.data)
                    return Future(value: response)
                } catch {
                    return Future(error: .deserialization)
                }
            }
    }

    // MARK: Private methods

    private func get(path: String, with parameters: [String: String]) -> Future<SuccessJSONResponse, ExchangeRateClientError> {
        let promise = Promise<SuccessJSONResponse, ExchangeRateClientError>()

        networkClient.get(path, parameters: parameters) { result in
            switch result {
            case let .success(jsonResponse):
                promise.success(jsonResponse)
            case let .failure(jsonResponse):
                promise.failure(.other(jsonResponse.error))
            }
        }

        return promise.future
    }
}
