import Foundation

final class AppDependencies {

    // MARK: Internal properties

    let exchangeRateClient: ExchangeRateClientType

    // MARK: Lifecycle

    init(appConstants: AppConstants) {
        self.exchangeRateClient = FixerClient(appConstants: appConstants)
    }
}

extension AppDependencies: HasExchangeRateClient {}
