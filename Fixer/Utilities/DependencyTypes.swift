import Foundation

protocol HasExchangeRateClient {
    var exchangeRateClient: ExchangeRateClientType { get }
}
