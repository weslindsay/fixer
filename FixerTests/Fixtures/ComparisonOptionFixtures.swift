import Foundation

@testable import Fixer

extension ComparisonOption {

    static func testComparisonOption(baseAmount: Decimal = 1) -> ComparisonOption {
        return ComparisonOption(
            baseCurrency: .EUR,
            baseAmount: baseAmount,
            lhsCurrency: .AUD,
            rhsCurrency: .CAD
        )
    }
}
