import Foundation

@testable import Fixer

extension RatesResponse {

    static func emptyTestResponse() -> RatesResponse {
        return RatesResponse(
            success: true,
            timestamp: 1586028618,
            date: "2020-04-04",
            base: "EUR",
            rates: [:]
        )
    }

    static func successfulTestResponse() -> RatesResponse {
        return RatesResponse(
            success: true,
            timestamp: 1586028618,
            date: "2020-04-04",
            base: "EUR",
            rates: [
                "USD": 1.10254,
                "JPY": 120.489431,
                "GBP": 0.843298,
                "AUD": 1.615455,
                "CAD": 1.449565,
                "CHF": 1.070999,
                "CNY": 7.647994,
                "SEK": 10.53849,
                "NZD": 1.668114
            ]
        )
    }

    static func failedTestResponse() -> RatesResponse {
        return RatesResponse(
            success: false,
            timestamp: 1586028618,
            date: "2020-04-04",
            base: "EUR",
            rates: [
                "USD": 1.10254,
                "JPY": 120.489431,
                "GBP": 0.843298,
                "AUD": 1.615455,
                "CAD": 1.449565,
                "CHF": 1.070999,
                "CNY": 7.647994,
                "SEK": 10.53849,
                "NZD": 1.668114
            ]
        )
    }

    static func unknownTestResponse() -> RatesResponse {
        return RatesResponse(
            success: true,
            timestamp: 1586028618,
            date: "2020-04-04",
            base: "AAA",
            rates: [
                "USD": 1.10254,
                "JPY": 120.489431,
                "GBP": 0.843298,
                "AUD": 1.615455,
                "CAD": 1.449565,
                "CHF": 1.070999,
                "CNY": 7.647994,
                "SEK": 10.53849,
                "NZD": 1.668114
            ]
        )
    }

    static func successfulHistoryResponse(date: String) -> RatesResponse {
        return RatesResponse(
            success: true,
            timestamp: 1586028618,
            date: date,
            base: "EUR",
            rates: [
                "AUD": 1.615455,
                "CAD": 1.449565
            ]
        )
    }

    static func unsuccessfulHistoryResponse(date: String) -> RatesResponse {
        return RatesResponse(
            success: false,
            timestamp: 1586028618,
            date: date,
            base: "EUR",
            rates: [
                "AUD": 1.615455,
                "CAD": 1.449565
            ]
        )
    }

    static func successfulHistoryResponses() -> [RatesResponse] {
        return [
            self.successfulHistoryResponse(date: "2020-04-03"),
            self.successfulHistoryResponse(date: "2020-04-02"),
            self.successfulHistoryResponse(date: "2020-04-01"),
            self.successfulHistoryResponse(date: "2020-03-31"),
            self.successfulHistoryResponse(date: "2020-03-30"),
        ]
    }

    static func unsuccessfulHistoryResponses() -> [RatesResponse] {
        return [
            self.unsuccessfulHistoryResponse(date: "2020-04-03"),
            self.successfulHistoryResponse(date: "2020-04-02"),
            self.successfulHistoryResponse(date: "2020-04-01"),
            self.successfulHistoryResponse(date: "2020-03-31"),
            self.successfulHistoryResponse(date: "2020-03-30"),
        ]
    }
}
