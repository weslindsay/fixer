import BrightFutures
import Foundation

@testable import Fixer

final class MockExchangeRateClient: ExchangeRateClientType {

    var successfulRatesResponse: Decodable?
    var successfulHistoricalResponses: [Decodable]?

    // MARK: ExchangeRateClientType methods

    func requestLatestRates<Response>(for currencies: [Currency], using baseCurrency: Currency) -> Future<Response, ExchangeRateClientError> where Response : Decodable {
        guard let response = successfulRatesResponse as? Response else {
            return Future(error: .deserialization)
        }

        return Future(value: response)
    }

    func requestHistoricalRates<Response>(on date: Date, for firstCurrency: Currency, and secondCurrency: Currency, using baseCurrency: Currency) -> Future<Response, ExchangeRateClientError> where Response : Decodable {
        guard var historicalResponses = successfulHistoricalResponses else {
            return Future(error: .deserialization)
        }

        guard let response = historicalResponses.first as? Response else {
            return Future(error: .deserialization)
        }

        historicalResponses.removeFirst()
        successfulHistoricalResponses = historicalResponses

        return Future(value: response)
    }
}
