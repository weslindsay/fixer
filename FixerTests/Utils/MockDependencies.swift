import Foundation

@testable import Fixer

final class MockDependencies {

    // MARK: Internal properties

    let mockExchangeRateClient: MockExchangeRateClient

    // MARK: Lifecycle

    init(mockExchangeRateClient: MockExchangeRateClient = MockExchangeRateClient()) {
        self.mockExchangeRateClient = mockExchangeRateClient
    }
}

extension MockDependencies: HasExchangeRateClient {
    var exchangeRateClient: ExchangeRateClientType {
        return mockExchangeRateClient
    }
}
