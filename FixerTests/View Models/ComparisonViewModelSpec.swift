import Nimble
import Quick
import RxSwift

@testable import Fixer

final class ComparisonViewModelSpec: QuickSpec {
    override func spec() {

        let disposeBag = DisposeBag()
        let dateFormatter = DateFormatter()
        var subject: ComparisonViewModel!
        var mockDependencies: MockDependencies!

        beforeEach {
            mockDependencies = MockDependencies()

            dateFormatter.dateFormat = "yyyy-MM-dd"
        }

        describe("#refreshRates") {
            beforeEach {
                let date = dateFormatter.date(from: "2020-04-04")!
                let comparisonOption = ComparisonOption.testComparisonOption()

                subject = ComparisonViewModel(
                    dependencies: mockDependencies,
                    comparisonOption: comparisonOption,
                    startDate: date
                )
            }

            context("when the network request is successful") {
                beforeEach {
                    let historicalResponses = RatesResponse.successfulHistoryResponses()
                    mockDependencies.mockExchangeRateClient.successfulHistoricalResponses = historicalResponses
                }

                it("returns success") {
                    var isSuccessful = false
                    subject
                        .refreshRates()
                        .onSuccess(callback: { _ in
                            isSuccessful = true
                        })

                    expect(isSuccessful).toEventually(beTrue())
                }
            }

            context("when one of the history requests are unsuccessful") {
                beforeEach {
                    let historicalResponses = RatesResponse.unsuccessfulHistoryResponses()
                    mockDependencies.mockExchangeRateClient.successfulHistoricalResponses = historicalResponses
                }

                it("returns failure with a server error") {
                    var returnedError: ComparisonViewModel.ComparisonError!
                    subject
                        .refreshRates()
                        .onFailure(callback: { error in
                            returnedError = error
                        })
                    expect(returnedError).toEventually(equal(ComparisonViewModel.ComparisonError.serverError))
                }
            }
        }

        describe("#sectionCount") {
            beforeEach {
                let date = dateFormatter.date(from: "2020-04-04")!
                let comparisonOption = ComparisonOption.testComparisonOption()

                subject = ComparisonViewModel(
                    dependencies: mockDependencies,
                    comparisonOption: comparisonOption,
                    startDate: date
                )
            }

            context("when the network request is successful") {
                beforeEach {
                    let historicalResponses = RatesResponse.successfulHistoryResponses()
                    mockDependencies.mockExchangeRateClient.successfulHistoricalResponses = historicalResponses

                    waitUntil { done in
                        subject.refreshRates().onComplete(callback: { _ in done() })
                    }
                }

                it("returns a count of 5") {
                    expect(subject.sectionCount).toEventually(equal(5))
                }
            }

            context("when the network request is unsuccessful") {
                beforeEach {
                    let historicalResponses = RatesResponse.unsuccessfulHistoryResponses()
                    mockDependencies.mockExchangeRateClient.successfulHistoricalResponses = historicalResponses

                    waitUntil { done in
                        subject.refreshRates().onComplete(callback: { _ in done() })
                    }
                }

                it("returns a count of 0") {
                    expect(subject.sectionCount).toEventually(equal(0))
                }
            }
        }

        describe("#sections") {

            context("when the network request is successful") {

                beforeEach {
                    let historicalResponses = RatesResponse.successfulHistoryResponses()
                    mockDependencies.mockExchangeRateClient.successfulHistoricalResponses = historicalResponses
                }

                context("and the base currency value is '1'") {
                    beforeEach {
                        let date = dateFormatter.date(from: "2020-04-04")!
                        let comparisonOption = ComparisonOption.testComparisonOption(baseAmount: 1)

                        subject = ComparisonViewModel(
                            dependencies: mockDependencies,
                            comparisonOption: comparisonOption,
                            startDate: date
                        )

                        waitUntil { done in
                            subject.refreshRates().onComplete(callback: { _ in done() })
                        }
                    }

                    it("retuns the sections in descending order") {
                        var returnedSections = [ComparisonLayoutSection]()

                        subject
                            .sections
                            .subscribe(onNext: { sections in
                                returnedSections = sections
                            })
                            .disposed(by: disposeBag)

                        expect(returnedSections.count).toEventually(equal(5))

                        let firstSection = returnedSections.first!
                        expect(firstSection.title).toEventually(equal("Apr 3, 2020"))

                        let firstRowData = firstSection.rows.first!
                        expect(firstRowData.lhsCurrencyTitle).toEventually(equal("AUD"))
                        expect(firstRowData.rhsCurrencyTitle).toEventually(equal("CAD"))

                        expect(firstRowData.lhsCurrencyValue).toEventually(equal("1.62"))
                        expect(firstRowData.rhsCurrencyValue).toEventually(equal("1.45"))

                        expect(firstRowData.lhsFlagImage).toEventuallyNot(beNil())
                        expect(firstRowData.rhsFlagImage).toEventuallyNot(beNil())

                        let lastSection = returnedSections.last!
                        expect(lastSection.title).toEventually(equal("Mar 30, 2020"))

                        let lastRowData = lastSection.rows.first!
                        expect(lastRowData.lhsCurrencyTitle).toEventually(equal("AUD"))
                        expect(lastRowData.rhsCurrencyTitle).toEventually(equal("CAD"))

                        expect(lastRowData.lhsCurrencyValue).toEventually(equal("1.62"))
                        expect(lastRowData.rhsCurrencyValue).toEventually(equal("1.45"))

                        expect(lastRowData.lhsFlagImage).toEventuallyNot(beNil())
                        expect(lastRowData.rhsFlagImage).toEventuallyNot(beNil())
                    }
                }

                context("and the base currency value is '11.23'") {
                    beforeEach {
                        let date = dateFormatter.date(from: "2020-04-04")!
                        let comparisonOption = ComparisonOption.testComparisonOption(baseAmount: 11.23)

                        subject = ComparisonViewModel(
                            dependencies: mockDependencies,
                            comparisonOption: comparisonOption,
                            startDate: date
                        )

                        waitUntil { done in
                            subject.refreshRates().onComplete(callback: { _ in done() })
                        }
                    }

                    it("retuns the sections in descending order") {
                        var returnedSections = [ComparisonLayoutSection]()

                        subject
                            .sections
                            .subscribe(onNext: { sections in
                                returnedSections = sections
                            })
                            .disposed(by: disposeBag)

                        expect(returnedSections.count).toEventually(equal(5))

                        let firstSection = returnedSections.first!
                        expect(firstSection.title).toEventually(equal("Apr 3, 2020"))

                        let firstRowData = firstSection.rows.first!
                        expect(firstRowData.lhsCurrencyTitle).toEventually(equal("AUD"))
                        expect(firstRowData.rhsCurrencyTitle).toEventually(equal("CAD"))

                        expect(firstRowData.lhsCurrencyValue).toEventually(equal("18.14"))
                        expect(firstRowData.rhsCurrencyValue).toEventually(equal("16.28"))

                        expect(firstRowData.lhsFlagImage).toEventuallyNot(beNil())
                        expect(firstRowData.rhsFlagImage).toEventuallyNot(beNil())

                        let lastSection = returnedSections.last!
                        expect(lastSection.title).toEventually(equal("Mar 30, 2020"))

                        let lastRowData = lastSection.rows.first!
                        expect(lastRowData.lhsCurrencyTitle).toEventually(equal("AUD"))
                        expect(lastRowData.rhsCurrencyTitle).toEventually(equal("CAD"))

                        expect(lastRowData.lhsCurrencyValue).toEventually(equal("18.14"))
                        expect(lastRowData.rhsCurrencyValue).toEventually(equal("16.28"))

                        expect(lastRowData.lhsFlagImage).toEventuallyNot(beNil())
                        expect(lastRowData.rhsFlagImage).toEventuallyNot(beNil())
                    }
                }
            }
        }
    }
}
