import Nimble
import Quick
import RxSwift

@testable import Fixer

final class LatestRatesViewModelSpec: QuickSpec {
    override func spec() {

        let disposeBag = DisposeBag()
        var subject: LatestRatesViewModel!
        var mockDependencies: MockDependencies!

        beforeEach {
            mockDependencies = MockDependencies()
        }

        describe("#refreshRates") {
            beforeEach {
                subject = LatestRatesViewModel(dependencies: mockDependencies)
            }

            context("when the network request is successful") {
                beforeEach {
                    let response = RatesResponse.successfulTestResponse()
                    mockDependencies.mockExchangeRateClient.successfulRatesResponse = response
                }

                it("returns success") {
                    var isSuccessful = false
                    subject
                        .refreshRates()
                        .onSuccess(callback: { _ in
                            isSuccessful = true
                        })

                    expect(isSuccessful).toEventually(beTrue())
                }
            }

            context("when the network request is unsuccessful") {
                beforeEach {
                    let response = RatesResponse.failedTestResponse()
                    mockDependencies.mockExchangeRateClient.successfulRatesResponse = response
                }

                it("returns failure with a server error") {
                    var returnedError: LatestRatesViewModel.LatestRatesError!
                    subject
                        .refreshRates()
                        .onFailure(callback: { error in
                            returnedError = error
                        })
                    expect(returnedError).toEventually(equal(LatestRatesViewModel.LatestRatesError.serverError))
                }
            }

            context("when the base currency returned is not recognised") {
                beforeEach {
                    let response = RatesResponse.unknownTestResponse()
                    mockDependencies.mockExchangeRateClient.successfulRatesResponse = response
                }

                it("returns success") {
                    var returnedError: LatestRatesViewModel.LatestRatesError!
                    subject
                        .refreshRates()
                        .onFailure(callback: { error in
                            returnedError = error
                        })
                    expect(returnedError).toEventually(equal(LatestRatesViewModel.LatestRatesError.baseRateUnavailable))
                }
            }
        }

        describe("#rows") {
            beforeEach {
                subject = LatestRatesViewModel(dependencies: mockDependencies)
            }

            context("when the network request is successful") {

                beforeEach {
                    let response = RatesResponse.successfulTestResponse()
                    mockDependencies.mockExchangeRateClient.successfulRatesResponse = response

                    waitUntil { done in
                        subject.refreshRates().onComplete(callback: { _ in done() })
                    }
                }

                context("and the base currency input is '1'") {
                    beforeEach {
                        subject.baseCurrencyInput.accept("1")
                    }

                    it("retuns the currency data in alphabetical order") {
                        var returnedRows = [LatestRateTableViewCell.Data]()

                        subject
                            .rows
                            .subscribe(onNext: { rows in
                                returnedRows = rows
                            })
                            .disposed(by: disposeBag)

                        expect(returnedRows.count).toEventually(equal(9))

                        let audRow = returnedRows.first!
                        expect(audRow.currencyTitle).toEventually(equal("AUD"))
                        expect(audRow.currencyValue).toEventually(equal("1.62"))
                        expect(audRow.flagImage).toEventuallyNot(beNil())

                        let usdRow = returnedRows.last!
                        expect(usdRow.currencyTitle).toEventually(equal("USD"))
                        expect(usdRow.currencyValue).toEventually(equal("1.1"))
                        expect(usdRow.flagImage).toEventuallyNot(beNil())
                    }
                }

                context("and the base currency input is '2.35'") {
                    beforeEach {
                        subject.baseCurrencyInput.accept("2.35")
                    }

                    it("retuns the currency data multiplied by the base input in alphabetical order") {
                        var returnedRows = [LatestRateTableViewCell.Data]()

                        subject
                            .rows
                            .subscribe(onNext: { rows in
                                returnedRows = rows
                            })
                            .disposed(by: disposeBag)

                        expect(returnedRows.count).toEventually(equal(9))

                        let audRow = returnedRows.first!
                        expect(audRow.currencyTitle).toEventually(equal("AUD"))
                        expect(audRow.currencyValue).toEventually(equal("3.8"))
                        expect(audRow.flagImage).toEventuallyNot(beNil())

                        let usdRow = returnedRows.last!
                        expect(usdRow.currencyTitle).toEventually(equal("USD"))
                        expect(usdRow.currencyValue).toEventually(equal("2.59"))
                        expect(usdRow.flagImage).toEventuallyNot(beNil())
                    }
                }
            }
        }

        describe("#rowCount") {
            beforeEach {
                 subject = LatestRatesViewModel(dependencies: mockDependencies)
             }

            context("when the network request is successful") {
                beforeEach {
                    let response = RatesResponse.successfulTestResponse()
                    mockDependencies.mockExchangeRateClient.successfulRatesResponse = response

                    waitUntil { done in
                        subject.refreshRates().onComplete(callback: { _ in done() })
                    }
                }

                it("returns a count of 9") {
                    expect(subject.rowCount).toEventually(equal(9))
                }
            }

            context("when the network request has no results") {
                beforeEach {
                    let response = RatesResponse.emptyTestResponse()
                    mockDependencies.mockExchangeRateClient.successfulRatesResponse = response

                    waitUntil { done in
                        subject.refreshRates().onComplete(callback: { _ in done() })
                    }
                }

                it("returns a count of 0") {
                    expect(subject.rowCount).toEventually(equal(0))
                }
            }
        }

        describe("#toggleSelection") {
            beforeEach {
                subject = LatestRatesViewModel(dependencies: mockDependencies)
            }

            context("when the selection mode is `normal`") {
                it("changes to `selection`") {
                    expect(subject.currentSelectionMode) == .normal
                    subject.toggleSelection()
                    expect(subject.currentSelectionMode) == .selection
                }
            }

            context("when the selection mode is `selection`") {
                beforeEach {
                    subject = LatestRatesViewModel(dependencies: mockDependencies)
                    subject.toggleSelection()
                }

                it("changes to `normal`") {
                    expect(subject.currentSelectionMode) == .selection
                    subject.toggleSelection()
                    expect(subject.currentSelectionMode) == .normal
                }
            }
        }

        describe("#resetSelectionMode") {
            beforeEach {
                subject = LatestRatesViewModel(dependencies: mockDependencies)
            }

            context("when the selection mode is `normal`") {
                it("remains in `normal` mode") {
                    expect(subject.currentSelectionMode) == .normal
                    subject.resetSelectionMode()
                    expect(subject.currentSelectionMode) == .normal
                }
            }

            context("when the selection mode is `selection`") {
                beforeEach {
                    subject = LatestRatesViewModel(dependencies: mockDependencies)
                    subject.toggleSelection()
                }

                it("resets to `normal`") {
                    expect(subject.currentSelectionMode) == .selection
                    subject.resetSelectionMode()
                    expect(subject.currentSelectionMode) == .normal
                }
            }
        }

        describe("#compareButtonState") {
            beforeEach {
                subject = LatestRatesViewModel(dependencies: mockDependencies)
                let response = RatesResponse.successfulTestResponse()
                mockDependencies.mockExchangeRateClient.successfulRatesResponse = response

                waitUntil { done in
                    subject.refreshRates().onComplete(callback: { _ in done() })
                }
            }

            context("when there are no selected currencies") {
                it("returns `.disabled`") {
                    var compareButtonState: CompareButton.ButtonState!

                    subject
                        .compareButtonState
                        .subscribe(onNext: { state in
                            compareButtonState = state
                        })
                        .disposed(by: disposeBag)

                    expect(compareButtonState).toEventually(equal((.disabled)))
                }
            }

            context("when there is one selected currency") {
                beforeEach {
                    subject.selectCurrency(at: IndexPath(item: 0, section: 0))
                }

                it("returns `.disabled`") {
                    var compareButtonState: CompareButton.ButtonState!

                    subject
                        .compareButtonState
                        .subscribe(onNext: { state in
                            compareButtonState = state
                        })
                        .disposed(by: disposeBag)

                    expect(compareButtonState).toEventually(equal((.disabled)))
                }
            }

            context("when there are two selected currencies") {
                beforeEach {
                    subject.selectCurrency(at: IndexPath(item: 0, section: 0))
                    subject.selectCurrency(at: IndexPath(item: 1, section: 0))
                }

                it("returns `.enabled`") {
                    var compareButtonState: CompareButton.ButtonState!

                    subject
                        .compareButtonState
                        .subscribe(onNext: { state in
                            compareButtonState = state
                        })
                        .disposed(by: disposeBag)

                    expect(compareButtonState).toEventually(equal((.enabled)))
                }
            }
        }

        describe("#comparisonOption") {
            beforeEach {
                subject = LatestRatesViewModel(dependencies: mockDependencies)
                let response = RatesResponse.successfulTestResponse()
                mockDependencies.mockExchangeRateClient.successfulRatesResponse = response

                waitUntil { done in
                    subject.refreshRates().onComplete(callback: { _ in done() })
                }
            }

            context("when there are no selected currencies") {
                it("returns nil") {
                    expect(subject.comparisonOption()).to(beNil())
                }
            }

            context("when there is one selected currency") {
                beforeEach {
                    subject.selectCurrency(at: IndexPath(item: 0, section: 0))
                }

                it("returns nil") {
                    expect(subject.comparisonOption()).to(beNil())
                }
            }

            context("when there are two selected currencies") {
                beforeEach {
                    subject.selectCurrency(at: IndexPath(item: 0, section: 0))
                    subject.selectCurrency(at: IndexPath(item: 1, section: 0))
                }

                it("returns a comparisonOption") {
                    let comparisonOption = subject.comparisonOption()

                    expect(comparisonOption?.baseAmount) == 1
                    expect(comparisonOption?.baseCurrency) == .EUR
                    expect(comparisonOption?.lhsCurrency) == .AUD
                    expect(comparisonOption?.rhsCurrency) == .CAD
                }
            }
        }
    }
}
