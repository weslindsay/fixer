# Fixer

iOS application written in Swift to consume the [Fixer.io](https://fixer.io/) API.

## Getting Started

### Requirements

- XCode 11.3

### From Bitbucket

Clone the repository and open the Fixer.xcworkspace file in XCode.

Build and run the application.

### From Zip file

Open the Fixer.xcworkspace file in XCode.

Build and run the application.

## 3rd party dependencies

I used [CocoaPods](https://cocoapods.org/) for dependency management and the following 3rd party libraries:

- [Bright Futures](https://github.com/Thomvis/BrightFutures)
- [Networking](https://github.com/3lvis/Networking)
- [RXSwift](https://github.com/ReactiveX/RxSwift)
- [SnapKit](https://github.com/SnapKit/SnapKit)

- [Nimble](https://github.com/Quick/Nimble)
- [Quick](https://github.com/Quick/Quick)


## Further development

- UI tests with either KIF or XCUITest
- Rate limit refresh requests buy using the last updated time interval
- Add the ability to change the base currency
- Add the ability to update the comparison base value on the comparison screen

## License

This project is licensed under the [MIT License](https://bitbucket.org/weslindsay/fixer/src/master/License)

